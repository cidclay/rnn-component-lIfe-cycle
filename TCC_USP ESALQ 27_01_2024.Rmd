---
title: "TCC_USP ESALQ 25_01_2024"
author: "Cid Clay Quirino"
date: "2024-01-25"
output:
  html_document: default
  pdf_document: default
---

<span style="font-size: 16px;">

# Component Life Projection Based on a Deep Learning Model for Mobile Mining Equipment 

</span>

<span style="font-size: 14px;">

## 1.0	Introduction

</span>

Manufacturers and dealers around the world, are constantly approached by customers about how to consistently achieve best resulting in anticipation of component failures. One way to move forward on this issue is using the condition monitoring process output. In certain mining assets components (Ex: Diesel Engines/Transmission and major components) used in mobile equipment models this measurement can be done directly through “Electronic System” from online monitoring system data or from telemetry. 

In these cases, components have highly predictability parameters, but in other type of components like Cylinders, Pumps and minor components is not possible detect failures in fact from this path mainly due a few strong parameters to monitor.   

Hence the CMMS (Conditions Monitoring Management System) needs to be improved using  other practices to evaluate any changes in component behavior and acting before failure. On the other hand, engineering team need to be care to not have a high investment in comparison to cost saving. Some mining customer in the north of Brazil is seeing a good results applying the best practice developed jointly by Dealer – monitoring the behavior of cylinder parameters to prevent unplanned failure and reducing asset downtime.

This study intend the importance of this best practices, and wants improve this solution using Artificial Intelligence (AI) and Neural Network to get data directly from a sender installer on this minor components to detect very early any changes on temperature and vibration.



## 2.0	Project Description

### 2.1 Old project manual data colection and result

This best practice is part of Hydro’s and Sotreq’s effort to improve the monitoring and troubleshooting process for cylinders based on real case of CMMS - Condition Monitoring Management System application.

The development of alternative monitoring/troubleshooting was based on the customer’s need to reduce time to fix and improve the fleet availability/productivity.  

Thermography, along with other operational tests, provided the means to monitor and provide detection capabilities before catastrofic failure and helped define future intervention in the HEX, based on the HEX monitoring of component in tool cylinder of a 390F.


Basic Operation:  

There is literature that discribes the operational verification (KENR9995-07), cylinder drop - verification - empty bucket (KENR9995-00)"and cycle speed - verification (KENR9995-02) to evaluate the lifting cylinders of the boom.

Figure 1.  Fluid Temperature
![Figure 1.  Fluid Temperature](D:/RNN_Sensor Dynamox/6030_Cilindros e Redutores/Dados Sensor/Figure 1.  Fluid Temperature.png)

The equipment was positioned to perform the cylinder drop test (Figure 2), and the boom lift cylinder was measured from the rod fixing pin to the cylinder liner fixing pin, and after 5 minutes with the engine off, no displacement of the cylinder rod was identified through the measurement (the cylinders kept the same measurements).



Figure 2. Position of the drop test equipment

![Figure 2. Imagem Escavadeira Hydro Hidraulico Antes Falha](D:/RNN_Sensor Dynamox/6030_Cilindros e Redutores/Dados Sensor/Imagem Escavadeira Hydro Hidraulico Antes Falha.png)
Before performing the cycle speed test, the pump relief pressures were checked for the boom lift function, and found 5206 psi for the 01# pump and 5173 psi for the 02# pump (Figure 3).


Figure 3  - Pump Pressure Test

![Figure 3. Pump Pressure](D:/RNN_Sensor Dynamox/6030_Cilindros e Redutores/Dados Sensor/Figure 3. Pump Pressure.png)

Note: The pressures were a little above the specified, but not enough to change the test result.

The machine implement was positioned as oriented by CAT literature to perform the cylinder cycle speed test and the cylinder cycle time from soil to fully extended rod position, and ir was collected  7.26 seconds.

The cylinder speed procedure specifies a time of 5.8 ± 0.5 for a new cylinder, a maximum of 7.5 seconds for reconditioning the cylinder and a maximum of 8.7 seconds for service limit for the boom cylinder (Figure 05).




Figure 4  - Pump Pressure Test

![Figure 4  - Pump Pressure Test](D:/RNN_Sensor Dynamox/6030_Cilindros e Redutores/Dados Sensor/Figure 4  - Pump Pressure Test.png)


The procedure for setting the cycle time defined in Troubleshooting CAT was carried out, and the lifting time specified by the lower operation and maintenance manual was detected.

After finishing the cycle time and pressure tests, the thermography test was performed, with the aid of the thermographic camera, so that the differential temperature of the cylinders was recorded.

Using thermography on the EH3201, a thermal differential was identified in the boom cylinder on the right side, as can be seen in Figure 6a with a temperature differential of 4.7°C in relation to the left side.


Figure 5a. Thermography on the EH3201 Lift Cylinders

![Figure 5a. Thermography on the EH3201 Lift Cylinders](D:/RNN_Sensor Dynamox/6030_Cilindros e Redutores/Dados Sensor/Figure 5a. Thermography on the EH3201 Lift Cylinders.png)

Figure 5b. EH3201 on the operation front being inspected by predictive team

![Figure 5b. EH3201 on the operation front being inspected by predictive team](D:/RNN_Sensor Dynamox/6030_Cilindros e Redutores/Dados Sensor/Figure 5b. EH3201 on the operation front being inspected by predictive team.png)
The hottest points of the cylinders were captured by the camera and it was observed that the lifting cylinder l/d of the boom has a temperature of 5° c above the lifting cylinder l/e (Figure 7).

Figure 6.  Thermographic image indicating a 5° c difference between the hottest points of the boom lift cylinders

<div align="center">
![Figure 6.  Thermographic image indicating a 5° c difference between the hottest points of the boom lift cylinders](D:/RNN_Sensor Dynamox/6030_Cilindros e Redutores/Dados Sensor/Figure 6.  Thermographic image indicating a 5° c difference between the hottest points of the boom lift cylinders.png)
</div>

The CRC evaluation of the cylinder allowed us to relate the field symptoms and conclude that the thermographic method can be used to define with more assurance that the cylinder needed to be removed.


Figure 8.  Cyclinder at the CRC
![Figure 8.  Cyclinder at the CRC](D:/RNN_Sensor Dynamox/6030_Cilindros e Redutores/Dados Sensor/Figure 8.  Cyclinder at the CRC.png)

Next image shows some important details after disassembling the cylinder in remanufacture center, and evaluated internally evidences, confirm the relation from cylinder failure mode presented in field and causes after disassembled. See bellow that the failure mode presented in Figure 9, represent the cause os temperature increase in field termografic process. 

Figure 8a.  Images of disassemlbed cylinder at CRC 1

![Figure 8a.  Images of disassemlbed cylinder at CRC 1](D:/RNN_Sensor Dynamox/6030_Cilindros e Redutores/Dados Sensor/Figure 9.  Images of disassemlbed cylinder at CRC 1.png)
Figure 8b.  Images of disassemlbed cylinder at CRC 2

![Figure 8b.  Images of disassemlbed cylinder at CRC 2](D:/RNN_Sensor Dynamox/6030_Cilindros e Redutores/Dados Sensor/Figure 9.  Images of disassemlbed cylinder at CRC 2.png)

Figure 8c.  Images of disassemlbed cylinder at CRC 3
![Figure 8c.  Images of disassemlbed cylinder at CRC 3](D:/RNN_Sensor Dynamox/6030_Cilindros e Redutores/Dados Sensor/Figure 9.  Images of disassemlbed cylinder at CRC 3.png)
Based on this study, it was possible see the effectiveness of using this process to indicate possible internal oil leakage by the cylinder seal assembly. The Re manufacturing Center confirmed the issues with with cylinder.  Removing the cylinder based on the thermography along with the other tests was shown to be the proper course of action.


Figure 7a. New application on Major Mining Excavator
![Figure 7a. New application on Major Mining Excavator](D:/RNN_Sensor Dynamox/6030_Cilindros e Redutores/Dados Sensor/Figure 7a. New application on Major Mining Escavator.png)



## 3.0	Implementation Steps
Figure 7.  Project Datra Collection on CAT 6030

![Figure 7.  Project Datra Collection on CAT 6030](D:/RNN_Sensor Dynamox/6030_Cilindros e Redutores/Dados Sensor/Figure 7.  Project Datra Collection on CAT 6030.png)






Figure 8.  Exploration Data Analisys on CAT 6030

![Figure 8.  Exploration Data Analisys on CAT 6030](D:/RNN_Sensor Dynamox/6030_Cilindros e Redutores/Dados Sensor/Figure 8.  Exploration Data Analisys on CAT 6030.png)


Figure 9.  Component LIfe Projection on CAT 6030

![Figure 9.  Component LIfe Projection on CAT 6030](D:/RNN_Sensor Dynamox/6030_Cilindros e Redutores/Dados Sensor/Figure 9.  Component LIfe Projection on CAT 6030.png)



## 4.0	Benefits

Key benefits of implementing the best practice include the following:

Improved Machine Availability and Productivity– Components like cylinders that have few typical condition monitoring parameters can be better monitored with additional methods like thermography; replacement plans (parts, scheduled downtime, etc) can be put in place before failure of the component.

Additionally, these are direct and indirect benefits:
•	Allows greater agility in temperature assessment (lower MTTR)
•	Reduces risks through less time spent in the asset's operating zone (Elimination of live work)
•	Provides greater sensitivity in decision making for asset shutdown and definition of the next cycle time measurement step
•	Provided greater predictability in the cylinder replacement planning and schedule process;
o	Wear evaluation increase the accuracy in the replacement forecast;
o	Improvement in material stock planning by using the hours actually operated as a parameter;



```{r}
set.seed(1)
pacotes <- c("rattle","rnn","ggplot2","dplyr","tidyverse", "readxl", "knitr", "lubridate", "tidyr","reshape", "scales", "caret", "tools","tinytex")

if(sum(as.numeric(!pacotes %in% installed.packages())) != 0){
  instalador <- pacotes[!pacotes %in% installed.packages()]
  for(i in 1:length(instalador)) {
    install.packages(instalador, dependencies = T)
    break()}
  sapply(pacotes, require, character = T) 
} else {
  sapply(pacotes, require, character = T) 
}
library(readxl)
library(tools)
```

## R Markdown

This is an R Markdown document. Markdown is a simple formatting syntax for authoring HTML, PDF, and MS Word documents. For more details on using R Markdown see <http://rmarkdown.rstudio.com>.

When you click the **Knit** button a document will be generated that includes both content as well as the output of any embedded R code chunks within the document. You can embed an R code chunk like this:

```{r}
set.seed(1)
list.files()
arquivos <- list.files()
arquivos <- arquivos[grep(".xls", arquivos)]
for (j in arquivos){
  assign(strsplit(j, split="\\.")[[1]][1], read_excel(j))
}
BoomCylinder_LD <- BoomCylinder_LD %>% mutate(TagComp = "BoomCylinder_LD") %>% rename(replace = c("Tag" = "Parametro"))
BoomCylinder_LE <- BoomCylinder_LE %>% mutate(TagComp = "BoomCylinder_LE") %>% rename(replace = c("Tag" = "Parametro"))
BucketCylinder_LE <- BucketCylinder_LE %>% mutate(TagComp = "BucketCylinder_LE") %>% rename(replace = c("Tag" = "Parametro"))
BucketCylinder_LD <- BucketCylinder_LD %>% mutate(TagComp = "BucketCylinder_LD") %>% rename(replace = c("Tag" = "Parametro"))
BucketCylinder_LE <- BucketCylinder_LE %>% mutate(TagComp = "BucketCylinder_LE") %>% rename(replace = c("Tag" = "Parametro"))
FinalDrive_LD <- FinalDrive_LD %>% mutate(TagComp = "FinalDrive_LD") %>% rename(replace = c("Tag" = "Parametro"))
FinalDrive_LE <- FinalDrive_LE %>% mutate(TagComp = "FinalDrive_LE") %>% rename(replace = c("Tag" = "Parametro"))
SwingDrive_LD <- SwingDrive_LD %>% mutate(TagComp = "SwingDrive_LD") %>% rename(replace = c("Tag" = "Parametro"))
SwingDrive_LE <- SwingDrive_LE %>% mutate(TagComp = "SwingDrive_LE") %>% rename(replace = c("Tag" = "Parametro"))
PumpDrive_1 <- PumpDrive_1 %>% mutate(TagComp = "PumpDrive_1") %>% rename(replace = c("Tag" = "Parametro"))
PumpDrive_2 <- PumpDrive_2 %>% mutate(TagComp = "PumpDrive_2") %>% rename(replace = c("Tag" = "Parametro"))
ShellCylinder_LD <- ShellCylinder_LD %>% mutate(TagComp = "ShellCylinder_LD") %>% rename(replace = c("Tag" = "Parametro"))
ShellCylinder_LE <- ShellCylinder_LE %>% mutate(TagComp = "ShellCylinder_LE") %>% rename(replace = c("Tag" = "Parametro"))
StickCylinder_LD <- StickCylinder_LD %>% mutate(TagComp = "StickCylinder_LD") %>% rename(replace = c("Tag" = "Parametro"))
StickCylinder_LE <- StickCylinder_LE %>% mutate(TagComp = "StickCylinder_LE") %>% rename(replace = c("Tag" = "Parametro"))
SwingGear_1 <- SwingGear_1 %>% mutate(TagComp = "SwingGear_1") %>% rename(replace = c("Tag" = "Parametro"))
SwingGear_2 <- SwingGear_2 %>% mutate(TagComp = "SwingGear_2") %>% rename(replace = c("Tag" = "Parametro"))
BDadosRNN <- bind_rows(BoomCylinder_LD,BoomCylinder_LE,BucketCylinder_LD,BucketCylinder_LE,FinalDrive_LD,FinalDrive_LE,PumpDrive_1,PumpDrive_2,ShellCylinder_LD,ShellCylinder_LE,StickCylinder_LD,StickCylinder_LE,SwingDrive_LD,SwingDrive_LE,SwingGear_1,SwingGear_2)

BDadosRNN <- BDadosRNN %>%  mutate(Timestamp = dmy_hms(Timestamp), Data = as.Date(Timestamp), Hora = format(Timestamp, "%H:%M:%S"))
BDadosRNN <- BDadosRNN %>% select(-Timestamp)
rm(BoomCylinder_LD,BoomCylinder_LE,BucketCylinder_LD,BucketCylinder_LE,FinalDrive_LD,FinalDrive_LE,PumpDrive_1,PumpDrive_2,ShellCylinder_LD,ShellCylinder_LE,StickCylinder_LD,StickCylinder_LE,SwingDrive_LD,SwingDrive_LE,SwingGear_1,SwingGear_2)

```


You can also embed plots, for example:

```{r}
set.seed(1)
DfRnn <- BDadosRNN[, c(5,2)] %>% filter(BDadosRNN$Parametro == "Temperature" & BDadosRNN$TagComp == "BoomCylinder_LD")
DfRnn$Hora <- hms::as_hms(DfRnn$Hora)

ggplot(DfRnn, aes(x = Hora, y = Value)) +
  geom_line() +
  labs(title = "Gráfico de Dispersão com Horas",
       x = "Hora",
       y = "Valor") +
  theme_minimal()

#preparação dos dados
periodos_anteriores <- 3# Cada valor da serie temporal e uma função dos periodos anteriores.
n_col = ncol(DfRnn)

for (i in 1:periodos_anteriores) {
  for (j in 1:nrow(DfRnn))
    if (j - periodos_anteriores <= 0){
    } else {
      DfRnn [j, n_col + i] = DfRnn [j -i, 2]
    }
  }
DfRnn <- DfRnn[, 2:5]
DfRnn <- DfRnn[4:8003,]
names(DfRnn) <- c("y", "x_1", "x_2", "x_3")
```




Normalização
```{r}
set.seed(1)
#Normalizando o Df
normalize <- function(x)
{
  return((x - min(x))/ (max(x) - min(x)))
}
#MinMax Normalization
min <- min(DfRnn$y)
max <- max(DfRnn$y)
min <- min(DfRnn$x_1)
max <- max(DfRnn$x_1)
min <- min(DfRnn$x_2)
max <- max(DfRnn$x_2)
min <- min(DfRnn$x_3)
max <- max(DfRnn$x_3)

DFRnnNorm <-data.frame("Y" = (normalize(DfRnn$y)),"X_1" = (normalize(DfRnn$x_1)),"X_2"=(normalize(DfRnn$x_2)),"X_3"=(normalize(DfRnn$x_3)))

#DataFrame de Treino e teste
Y_array <- array(DFRnnNorm$Y, dim = c(nrow(DFRnnNorm) - 500, 1))
X_array <- array(c(DFRnnNorm$X_1[1:(nrow(DFRnnNorm)-500)],
                   DFRnnNorm$X_2[1:(nrow(DFRnnNorm)-500)],
                   DFRnnNorm$X_3[1:(nrow(DFRnnNorm)-500)]),
                 dim = c(nrow(DFRnnNorm) - 500, 1, 3))
```





Rede Neutal (RNN)
```{r, echo=FALSE}
# Treianmento da Rede Neural
TreinDfRnn_scaled = trainr(Y_array,X_array,
                           hidden_dim = 10,
                   learningrate = 0.2,
                   network_type = "rnn",
                   numepochs = 110)

#Plotando error Condition
```






```{r}
data.frame(Epoch = 5:length(attr(TreinDfRnn_scaled, "error")),
           Error = attr(TreinDfRnn_scaled, "error")[5:110]) %>% 
  ggplot(aes(x = Epoch,y = Error)) +
  geom_line(col = "red") +
  scale_x_continuous(breaks = seq(0, 110, 10),
                    labels = seq(0,110,10))

#Reverter Normalização
y <- DFRnnNorm$Y * (max - min) + min
x_1 <- DFRnnNorm$X_1 * (max - min) + min
x_2 <- DFRnnNorm$X_2 * (max - min) + min
x_3 <- DFRnnNorm$X_3 * (max - min) + min
DFRnnDesNorm <- data.frame("Y" = (y),"X_1" = (x_1),"X_2"=(x_2),"X_3"=(x_3))

entrada_h <- array(c(DFRnnNorm$X_1[1:(nrow(DFRnnNorm)-500):nrow(DFRnnNorm)],
                   DFRnnNorm$X_2[1:(nrow(DFRnnNorm)-500):nrow(DFRnnNorm)],
                   DFRnnNorm$X_3[1:(nrow(DFRnnNorm)-500):nrow(DFRnnNorm)]),
                 dim = c(nrow(DFRnnNorm)-(nrow(DFRnnNorm)- 500), 1, 3))

#Calculando previões com dados normalizados
pred_new1 <- predictr(TreinDfRnn_scaled, entrada_h, hidden = FALSE, real_output = T)
pred_old1 <- predictr(TreinDfRnn_scaled, X_array, hidden = FALSE, real_output = T)

#Revertendo normalização das previsões
pred_new1Y_NewDesnorm <- pred_new1 * (max - min) + min
pred_old1Y_OldDesnorm <- pred_old1 * (max - min) + min

FinalResult <- data.frame(real = DfRnn$y [1:7500],
                          pred = pred_old1Y_OldDesnorm)

FinalResult$id = 1:nrow(FinalResult)
FinalResult %>% gather(var, value, -id) -> FinalResult

FinalResult = rbind(FinalResult, data.frame(id = 7501:(7501+length(pred_new1Y_NewDesnorm)-1),
                              var = rep("new", length(pred_new1Y_NewDesnorm)),
                              value = pred_new1Y_NewDesnorm))
FinalResult %>% ggplot(aes(x = id,
                           y = value, colour = var))+
  geom_line(na.rm = T)+
  scale_colour_manual(values = c("Red", "Blue", "gray10"))
```



